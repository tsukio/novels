## 1.2.5 行列演算の性質
n次元ベクトルはn×1行列とみなせる

 <span style="color: red; ">注意！</span>
 縦ベクトル×横ベクトルと横ベクトル×縦ベクトルは違う  
 
$$
\begin{matrix}
a \\ 
b
\end{matrix}
 


$$