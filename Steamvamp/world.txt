〇題名
V for Vampire(Vは吸血鬼のV)(V for vendettaのパロ)

〇あらすじ（〇〇は決まってないところ）
吸血鬼達に支配され200年が経った20世紀初頭、イギリス。そこは赤い雲に覆われた蒸気と夜の国。
王政を打倒するために遣わされたエージェント〇〇は彼らに捕まりかけたところをを担いだ謎の男に助けられた。
「|串刺し公《カズィクル・ベイ》」と呼ばれる男と彼の担いでいた棺桶から現れた謎の少女、彼らの目的は奇しくも一致していた。
目指すはロンドン。狙うは大階差機関の秘密を握る三大公爵。
そして、イギリスすべてを支配する吸血鬼の王。
赤い雲の下、○○達の叛逆が始まる。

〇設定
・吸血鬼
赤い雲とともにイギリスに現れた怪物たち
そのスペックは多くは伝承通りだが、十字架などの特定の宗教由来の弱点はない
彼らは自らの形をある程度自由に変え、操ることができ、そのことが彼らの高い技術力の根源にもなっている
彼らはイギリスの旧王族の文化を引き継ぎ、吸血鬼の王を頂点とした王政がしかれている
吸血鬼の強さは体内の血液量によって決まっており、自分より強い吸血鬼には触れるだけで取り込まれてしまう可能性もある
その場合、自らの血を一点に集中させて武器や鎧を作り、飲み込まれないように戦う
（血液量が多い＝体が大きいというわけではない。吸血鬼たちは主に自らの密度を操作するため、見た目は普通でも膨大な血液量をもつ、ということもざら。大体BLEACHの霊圧みたいなもの）
（人間上がりの吸血鬼の方が変化や分裂がしにくい。人間の感覚が吸血鬼の体に追いつかないからである）
ゆえに吸血鬼の一番の弱点は吸血鬼であるともいえる

・食屍鬼(グール)
吸血鬼が己の血と人間から生み出す下級種族
常人よりはるかに力強く回復力も高いが、姿形を変えることはできず不老不死でもない
血液を失いすぎるか血液の効果がなくなる(吸血鬼・食屍鬼の血液と混ざる)と死ぬ
また、親(作成者)となる吸血鬼の命令に逆らうことができず、親が死ねばその配下の食屍鬼も死ぬ
吸血鬼が人体全てを赤い霧にコンバートしたものだとしたら、食屍鬼は血液のみをコンバートしたもの

・赤い雲
吸血鬼が現れるとともに出現した赤い雲
雲であるのになぜか風に流されず、イギリス上空に漂い続けている
これによって日光は完全に遮断され、イギリスは吸血鬼たちの伏魔殿となった
その発生源はロンドンにある大階差機関にある、と言われている

＊階差機関…J・H・ミュラーが考案、チャールズ・バベッジが発明した蒸気機関で動く計算機。お金と技術的な問題(小さい歯車が作れない)で研究されなくなった

・公害
イギリスは赤い雲や木の伐採により大地の循環機能が正常に機能しておらず、水分の大部分が赤く汚染されている
赤い水は飲み続けた人間は循環器系が弱り、また、免疫力が弱まってくる 
その影響でイギリスの人間は体が弱く、日光に当たらないせいかアルビノや障害を持った子供が生まれやすい
また、吸血鬼たちにとって赤い水を飲み続けた人間は食料としてもまずいらしい。同類の血を飲んでいるように感じるように感じるそうだ
よって吸血鬼たちは健康な人間を確保するため、国外の人間を狩りにいく。これが現在の吸血鬼たちの国外侵略の主な原因である。

・血の歯車
本作のイギリスでは蒸気機関を用いた技術が異様に発達しているが、その根源は血の歯車にあるといわれている
吸血鬼たちは自らの血を歯車などに固定することで、蒸気機関を用いた精密機器を作成している
血の歯車は擦り減らず、大きさも自由自在で、彼ら以外に作れないので技術の秘匿にもつながっている

・H2O情報通信理論
17世紀初頭にイギリスのエッセイ・ペーパーに匿名で寄稿された理論
内容は「水が情報を媒介する」というものであり、当時は一笑に付されていた
しかし、吸血鬼たちはこの理論を体感的に理解しており（吸血鬼は体のどの部分でも思考ができる＝体内の血液を情報媒体として用いている）、この理論を用いて蒸気文明を飛躍的に発達させた
吸血鬼がより強い吸血鬼の血によって自壊するのも、外部の情報によって自らの情報を保持できなくなるからであると言われている
碩学たちの間ではこの理論はサイエンスというよりスピリチュアルであり、魂の存在を証明するものだとされている

・階差機関
この世界の階差機関は吸血鬼たちが作り出した血の歯車とチャールズ・バベッジの生み出した超小型蒸気機関、H2O情報通信理論によって技術が飛躍的に発達している
仕組みとしては動力と入出力装置を蒸気機関を使用し、演算装置を吸血鬼が周囲に発生させた霧を用いて、通信装置に大気の水分を利用するのが主流である（吸血鬼の血を利用しないものもあるが、吸血鬼たちはあまり使わない）
現代のパソコン並みの性能があるH2Oコンピュータ、侵略用の無人飛行機、吸血鬼の目さえもごまかせる光学迷彩など、多くのものに階差機関が用いられている
しかしその技術は大衆（人間）には広まっておらず、多くの人間は中世の文化水準で生活している

・大階差機関
ロンドン中心にある超巨大H2Oコンピュータ。何人もの吸血鬼が血の歯車を提供して作り出したスーパーコンピューター
中核の部品は三大侯爵と呼ばれる王の家臣たちによって作られており、彼らが倒れない限りこの機械が壊れることはない
赤い雲をイギリスにとどめているとも、赤い雲につながっているともいわれているがその詳細は不明である

・新大陸
本来であればアメリカ合衆国と呼ばれた国。海を大きく隔てているため吸血鬼たちとの戦いが比較的少なく、人類圏でもっとも安全な土地
異様な発展を遂げたイギリスとは違い、内熱機関の発明や電気の発見など、史実に近い発展をしている
吸血鬼という人類の天敵がいるため、軍事技術の発達は史実の当時と比べて目覚ましい。肉体改造においては現代より進んでいる

・人間牧場
ロンドン地下深くにある巨大な施設。

〇登場人物
主人公勢
・「串刺し公」カズィクル・ベイ
「名前は捨てた。好きに呼べばいい」
主人公。吸血鬼たちの間で噂になっている「串刺し公(カズィクル・ベイ)」
棺桶型の釘打ち機を使い王政転覆のため吸血鬼を狩る人間の青年
幼い頃は吸血鬼たちに王子として育てられていたが、王の気分で捨てられたという過去を持つ
吸血鬼たちの食事になりそうだった所を彼の教育係によって救われ、辺境で息をひそめて暮らしていた
彼の目的は王への復讐それのみである
武装は巨大な棺桶型のパイルバンカー(釘打ち機)。棺桶型というか、子供が入るくらいの小さな棺桶が釘打ち機に括り付けられている
棺桶の中に吸血鬼を入れて、その血を釘を通じて打ち込むという拷問器具のような武器
さらに、中の吸血鬼の血を散布することで吸血鬼の演算機能を麻痺させる
一撃必殺とスリップダメージを兼ね備えたとんでも兵器
また、その霧を演算装置として用いるため脊髄に機械を繋げており、中の吸血鬼の演算能力を利用し敵の行動を予測する

・エヴァ
「ふくしゅーって、なんだか楽しそうじゃない？」
ベイと行動を共にする吸血鬼の少女
彼女は自らの血を操ることが出来ないが、王をも優に勝る血液量を誇っている
彼の持つ釘打ち機に入る生贄であり、ベイにとっての最大の武器
外見相応の幼さと外見に似つかわしくない残忍さを併せ持つ
好奇心旺盛で、自らの痛みさえ楽しんでいる節がある
エージェント曰く「価値観が生き物とズレている」

・アンネ・エイベル
「私は人類を救いにここに立っている。……だけど、１つだけ私情があります」
吸血鬼打倒のためにイギリスに潜入した新大陸出身の少女
冷静な判断力と肉体改造に由来する高い戦闘能力をもっている
彼女の武器は凝縮した吸血鬼の血液で作った「血の弾丸」
自分の命を人類のために捧げられる高潔な人間だが、それゆえに任務より目の前の命を優先してしまうことがある。要するにお人好し
幼い頃、吸血鬼たちに攫われた父がイギリスのどこかにいるかもしれないという淡い期待を抱いている
好きな食べ物はパンケーキ

・グレムリン
「私は妖精ほどのちんけな存在なのだよ」
ベイのパトロンである吸血鬼
パトロンであるが串刺し公と対面したことはなく、話もどこからともなく現れるコウモリの血の分体によって行われる
名前もアンネが呼び名を欲しがった時にはじめて口にしたものであり、十中八九偽名である
ベイのファンを自称しており、分体も特等席で串刺し公を見るためである
しかし、彼は観客であることに何らかの信条があるらしく、確信的な情報を与えたり、自ら力を振るうことはない
曰く「ベイが生まれたその時からのファン」らしいが、その態度がベイの過去の記憶を刺激し、彼を怒らせることも多い
どこにいても分体を自在に操ることができることから|貴い血族《ブルー・ブラッド》ではないかと推測されている
ちなみにグレムリンはイギリスの妖精で、機械にいたずらを仕掛けるという

三大公爵
・天眼卿サマセット
三大公爵の一人。イギリス全土の管理を担う
彼単体の戦闘能力は高くないが、自身の血を分けた分体を操ることに長けている
その能力を使い血をネズミや鳥などに変化させてイギリス全土に放っている。
彼は人間の管理の命令をこなし、趣味として楽しみ、使命と感じている
実働隊にからくり騎士という武装を複数持っているが、彼にとってそれはつまらない作業を効率的にこなすための道具にすぎない
彼の真価は人間への観察眼と階差機関を取り込んだことによる演算能力であり、相手と対面した場合その予測能力は未来予知と同義である

○大まかなプロット(第1章)
アンネが大階差機関の情報を知り、本国に通信しようとするが吸血鬼たちに追い詰められる
↓
ベイ登場、彼らを殲滅。アンネは気絶
↓
ベイのアジトで目をさますアンネ、互いの目的を確認し合う
↓
本国への通信のためにアンネたちの隠れ家に向かうが、隠れ家は吸血鬼たちに襲われていた
↓
襲われている仲間を助けるために吸血鬼たちと交戦。ここで三大公爵の一人、サマセットの武器であるからくり騎士と戦う
↓
からくり騎士を退けた一行。通信装置の移動・修復のための時間稼ぎと監視網を潰すためにサマセットの城へ向かう
↓
城では大量のからくり騎士がお出迎え、血を噴射して血の歯車を溶かすという荒技でからくり騎士を倒している一行
↓
サマセットと交戦、その演算能力に翻弄される一行
↓
アンネの放った弾丸に紛れていたグレムリンがサマセットの動きを止める。その隙に釘打ちきを打ち込むベイ
↓
勝利に喜ぶと同時に、分体一つでサマセットの動きを止めたグレムリンに警戒を高めるアンネ(了)

○小設定
・血膜

======以下旧設定====
テーマ「スカッとする話」「とにかくかっこいい」

登場人物
・味方
王子（主人公）
狼男
姫

・敵

・サブ
シスター



吸血貴(ブルー・ブラッド)に支配されたイギリス

赤い霧VS地球

赤い霧はH2Oを生命活動に必要とする星間飛行生物。通常は彗星の水分を吸収しているが、繁殖行動として子供を別銀河に飛ばす時に、惑星一つ分の水分を必要とする。

吸血貴は言わば消化器官赤い霧は防護の役割

吸血願望は赤い霧のせい

赤い水をのんだものは吸血鬼の命令に逆らえなくなる。

吸血鬼と人間の判別は一目ではつかない
一目でわかるのは狼男だけ

吸血鬼は等級ごとに己の髪を染める

・四天王的な？
「王子の顔を知るものは我々のみ」
王「披露前で吸血鬼にもなったおらんかったからな……」




雑魚騎士vs雑魚
・吸血鬼の基礎能力、兵器を解説
狼男登場、雑魚騎士撃破
・狼男の強さを見せつける
姫登場、いざ城へ
・
公爵とバベッジ会話
・聖書の伏線、公爵狼男を知ってモチベ上げる
城到着、城変形
公爵の所に到着
狼男、姫ピンチ。主人公姫の血液飲む
主人公、公爵にやられる。公爵姫の血で弱体化
狼男公爵撃破

狼男は狼王ロボのように無力化される
「懐かしいだろう？ーー君の妻の毛皮だ」
姫は血を流すには傷が必要

奴隷は聖書片手に王宮まで来た
姫「その子相当ロックね…」

バベッジ「流石にこいつは一世紀早い」「原子爆弾なんて」
バベッジは危機を感じた地球から未来の技術を教えられた




　２０世紀、イギリスは現在、吸血鬼に支配されていた。
　吸血貴《ブルー・ブラッド》。彼らは血を吸う怪物として語り継がれ、一時期は伝承が消滅していたものの、古来より人々に恐れられてきた存在である。
　しかしあくまで伝承。存在の有無はともかく、彼らは夜の暗がりから連想されるような不確かな者たちだった。

　彼らが世界史に台頭したのは19世紀初頭、イギリス産業革命の真っ只中であった。彼らは突然姿を現し、その
　



　「くっそ、気持ちよさそうに眠りやがって……」

　体力の限界だったのだろう。そう男は結論付けた。少年が自分たちと足並みを揃えられる健脚も、半日の間走り続ける体力を持ち合わせていない事を男は長い付き合いから知っていた。こうして男に抱えられているのさえ、彼には相当な体力を使わせているはずだ。

　
　
　
　
　
　部下は一人は聖書を暗唱してるし一人は王子をほっといて逃げ出そうと言いながら戦う

　王子「公爵の城にはマンチェスター行きの専用車線が敷かれています」「貴方達は列車の動かし方を知っていますか？　僕はマニュアルだけなら全て諳んじています」
　

「自滅覚悟、摂氏3000度の炎の斬撃だ」

剣が鎧に変形
パイルバンカーにはオリハルコンが使われているので磨耗しない

シスターがいるのは

○19世紀イギリスメモ
・ロンドンに都市計画と呼べる都市計画はなかった。
19世紀ロンドンのインフラでもっとも発達したのは地下である。
下水、郵便、水道、通信が気圧や水道を使って稼働し、乗り物にも使われていた。
->地下の整備は人間にさせられてそう
->主人公のカバージョブは配管工？
->地上は逆に区画ごと一掃とかされるかも

・都市部は辻馬車や乗合馬車が密集していた
->両方無人蒸気車になっているだろう

・